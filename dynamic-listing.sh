#!/bin/bash

ScriptDir=$(which $0 | sed "s/$(echo $0 | cut -c 3-)//") #Where are we running dynamic-listing.sh from?

Rewrite()
{
	
	HtmlTargetDirectory="$1" #$1 = Participant
	ShortTD=$(echo $HtmlTargetDirectory | sed 's|/var/www/html||') #Short Target Directory: Full path minus /var/www/html
	HtmlFile="$HtmlTargetDirectory/index.html"
	cp -f $ScriptDir/template.html "$HtmlTargetDirectory/index.html"
	cp -f $ScriptDir/template.css "$HtmlTargetDirectory/style.css"
	
	sed -i "s|<h1>.*|    <h1>[kevin@clarkweb]\$ ls $ShortTD</h1>|" $HtmlFile
	sed -i "s|<title>.*|    <title>$ShortTD</title>|" $HtmlFile
	sed -i '/<li><a href=/,$d' $HtmlFile

	echo "          <li><a href=\"..\">..</a></li>" >> $HtmlFile
	for FindList in $(find $HtmlTargetDirectory -maxdepth 1 -mindepth 1)
	do
		SubDirectory=$(echo $FindList | sed 's|^.*/||g')  #Can also be a file
		
		if [ "$SubDirectory" == "index.html" ] || [ "$SubDirectory" == "style.css" ]
		then	
			true #equivalent of python's "pass"
		elif [ $(stat "$FindList" | grep 'Access:' | head -1 | cut -c15) == 'd' ]
		then #This is a directory
			echo "          <li><a href=\"./$SubDirectory\">$SubDirectory</a></li>" >> $HtmlFile 
		else #This is a regular file
			echo "          <li><a href=\"./$SubDirectory\" class=\"file\">$SubDirectory</a></li>" >> $HtmlFile
		fi
		
	done
	echo "        </ul>" >> $HtmlFile
	echo "      </div>" >> $HtmlFile
	echo "  </body>" >> $HtmlFile
	echo "</html>" >> $HtmlFile
}

#Read participation file and loop for each entry
for Participant in $(cat directory-participation-list)
do
	Rewrite "$Participant"
done